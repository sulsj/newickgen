#!/bin/bash
set -ex
topdir=$(pwd)
#echo "Build all..."
#cd hashrfpq/hashrfpq && make clean && ./configure && make  && cd ${topdir}
#cd newickgen/newickgen &&  make clean && ./configure && make  && cd ${topdir}
#echo "Test run..."
#echo "Test run (1)..."
#python newickgen.py ./test/test.tre test-5-w-bl.tre 5 1
#more test-5-w-bl.tre
#python newickgen.py ./test/test.tre test-5-wo-bl.tre 5 0
#more test-5-wo-bl.tre

echo "Test run (2)..."
python p_arrange.py ./test/test2.tre 12 .5 out1.tre 1
more out1.tre
python p_arrange.py ./test/test2.tre 12 .5 out2.tre 0
more out2.tre
#python p_arrange.py ./test/test2.tre 10 0.5 out3.tre 1
#more out3.tre
#python p_arrange.py ./test/test2.tre 10 0.5 out4.tre 0
#more out4.tre
#python p_arrange.py ./test/test2.tre 10 1 out5.tre 1
#more out5.tre
#python p_arrange.py ./test/test2.tre 10 1 out6.tre 0
#more out6.tre
#python p_arrange.py ./test/test2.tre 10 1.0 out7.tre 1
#more out7.tre
#python p_arrange.py ./test/test2.tre 10 1.0 out8.tre 0
#more out8.tre
