#!/usr/bin/env python

import subprocess
import string
import commands
from sys import argv
from time import time
from math import sqrt
import sys

newickgen="./newickgen/newickgen/newickgen "
hashrfpq="./hashrfpq/hashrfpq/hashrfpq "


def read_trees(fileName, inputTrees):
  File = open(fileName,"r")
  numline = 1
  for l in File:
    if len(l) == 0:
      break
    inputTrees.append(l)
    numline += 1
  #print "line =", numline      
  File.close()
  #print "num trees in InputTrees =",len(inputTrees)
  return len(inputTrees)
  
  
if __name__ == "__main__":
    
    if len(argv) < 5:
        print "usage: %s input-tree-file out-tree-file num_tree print_branch_length" % argv[0]
        sys.exit(0)
        
    in_tree_file = argv[1]
    out_tree_file = argv[2]
    num_tree = int(argv[3])
    bBranchLen = int(argv[4])
    print "num tree = ", num_tree
    outtrees = []    
    for iii in range(0,num_tree):
        
        if (bBranchLen):
            cmd = newickgen + " " + in_tree_file + " -l "
        else: 
            cmd = newickgen + " " + in_tree_file
        proc = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE,)
        stdout_value = proc.communicate()[0]
        s = stdout_value.split()
        #for i in range(len(s)):
            #print str(i) + " " + str(s[i])
                   
        outtrees2 = []
        numOuttree = read_trees("outtree", outtrees2)
        if (iii == 0):              
            if (numOuttree == 1):
                outFile = open(out_tree_file, "w")
                outFile.write(outtrees2[0])
                outFile.close()                 
            else:
                print "ERROR: no tree in outtree"
                sys.exit(0)
        else:
            cmd2 = hashrfpq + " outtree " + out_tree_file + " 1 1"
            proc = subprocess.Popen(cmd2, shell=True,stdout=subprocess.PIPE,)
            stdout_value = proc.communicate()[0]
            s = stdout_value.split()
           #for i in range(len(s)):
               #print str(i) + " " + str(s[i])

            if (str(s[47]) == "0"):             
                outFile = open(out_tree_file, "a")
                outFile.write(outtrees2[0])
                outFile.close() 
            else:
                print "Same tree!"
                iii = iii - 1   
 
        print iii
                
    print "Completed!"
            
