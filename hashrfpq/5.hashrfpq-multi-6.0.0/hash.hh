///*****************************************************/
//
// Copyright (C) 2006, 2007 Seung-Jin Sul
// 		
//		Department of Computer Science
// 		Texas A&M University
// 		Contact: sulsj@cs.tamu.edu
//
// 		CLASS DEFINITION
//		HashRFMap: Class for hashing bitstrings
//
///*****************************************************/

#ifndef HASHRFMYHASH_HH
#define HASHRFMYHASH_HH

//#include <ext/hash_map>
#include <map>
#include <vector>

//using namespace __gnu_cxx;
using namespace std;

#include "bitset.hh"
#include "hashfunc.hh"

typedef struct {
	unsigned long	_hv2;
	unsigned int 	_t_i;
	float 			_dist;	
} BUCKET_STRUCT_T;

typedef vector<BUCKET_STRUCT_T> V_BUCKET_T;
typedef vector<V_BUCKET_T> HASHTAB_T;

typedef struct {
  unsigned long	_hv2;
  vector<unsigned> 	_vec_treeidx;
  vector<float>    _vec_dist;
} TREEIDX_STRUCT_T;

typedef vector<TREEIDX_STRUCT_T> V_BUCKET_T2;
typedef vector<V_BUCKET_T2> HASHTAB_T2;
	

class HashRFMap {	
	
public:
	HashRFMap() {}
	~HashRFMap() {}
	
	
	CHashFunc 	_HF;	
	//HASHTAB_T 	_hashtab; // for weighted
	HASHTAB_T2 	_hashtab2;
		
  void hashing_bs_without_type2_nbits(unsigned tree_i, unsigned num_taxa, unsigned long hv1, unsigned long hv2, float dist, bool w_option); // fast hash-rf
  void hashing_bs_without_type2_nbits_check(unsigned tree_i, unsigned num_taxa, unsigned long hv1, unsigned long hv2, float dist, bool w_option, vector< vector<float> > & sim); // fast hash-rf
  void uhashfunc_init(unsigned long t, unsigned long n, float r, unsigned long c);	
  void uhashfunc_init(unsigned long t, unsigned long n, float r, unsigned long c, int32 newseed);	
  void hashrfmap_clear();
  bool exist(unsigned long hv1, unsigned long hv2, unsigned treeIdx);
  bool exist(unsigned long hv1, unsigned long hv2);	
  void print_hashtable();
};


#endif

