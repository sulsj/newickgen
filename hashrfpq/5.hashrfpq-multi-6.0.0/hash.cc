/* This source has been formatted by an unregistered SourceFormatX */
/* If you want to remove this info, please register this shareware */
/* Please visit http://www.textrush.com to get more information    */

///*****************************************************/
//
// Copyright (C) 2006, 2007 Seung-Jin Sul
// 		Department of Computer Science
// 		Texas A&M University
// 		Contact: sulsj@cs.tamu.edu
//
// 		CLASS IMPLEMENTATION
//		HashRFMap: Class for hashing bit
//
///*****************************************************/

#include "hash.hh"
#include "hashfunc.hh"
#include <algorithm>
bool HashRFMap::exist(
        unsigned long hv1,
        unsigned long hv2,
        unsigned treeIdx) {

    unsigned sizeVec = _hashtab2[hv1].size();
    bool found = false;

    if (sizeVec) {
        for (unsigned i = 0; i < sizeVec; ++i) {
            if (_hashtab2[hv1][i]._hv2 == hv2) {
                if (std::find(_hashtab2[hv1][i]._vec_treeidx.begin(),
                        _hashtab2[hv1][i]._vec_treeidx.end(), treeIdx) !=
                        _hashtab2[hv1][i]._vec_treeidx.end()) {
                    found = true;
                    break;
                }
            }
        }
    }

    return found;
}

bool HashRFMap::exist(
        unsigned long hv1,
        unsigned long hv2) {

    unsigned sizeVec = _hashtab2[hv1].size();
    bool found = false;

    if (sizeVec) {
        for (unsigned i = 0; i < sizeVec; ++i) {
            if (_hashtab2[hv1][i]._hv2 == hv2) {

                found = true;
                break;
            }
        }
    }

    return found;
}

void HashRFMap::hashing_bs_without_type2_nbits(
        unsigned treeIdx,
        unsigned numTaxa,
        unsigned long hv1,
        unsigned long hv2,
        float dist,
        bool w_option) {
        	
    ////////////////////////////////////////////////////////////////////
    // double linked list
    ////////////////////////////////////////////////////////////////////
    // 1. search hv1 in the hashtab2
    // 2. if hv1 exists
    //       search hv2
    //       if hv2 exists
    //					insert treeIdx at the index of the linked list
    //       else
    //          insert {hv2, treeIdx} at the end of the linked list
    //     else
    //				make new list with {hv2, treeIdx} 
    //        and insert the list at HT[hv1]
    ////////////////////////////////////////////////////////////////////
    unsigned sizeVec = _hashtab2[hv1].size();

    if (sizeVec > 0) {
        bool found = false;
        for (unsigned i = 0; i < sizeVec; ++i) {
            if (_hashtab2[hv1][i]._hv2 == hv2) {
                _hashtab2[hv1][i]._vec_treeidx.push_back(treeIdx);
                if (w_option)
                    _hashtab2[hv1][i]._vec_dist.push_back(dist);
                found = true;
                break;
            }
        }
        if (!found) {
            TREEIDX_STRUCT_T bk2;
            bk2._hv2 = hv2;
            bk2._vec_treeidx.push_back(treeIdx);
            if (w_option)
                bk2._vec_dist.push_back(dist);
            _hashtab2[hv1].push_back(bk2);
        }
    } else if (sizeVec == 0) {
        TREEIDX_STRUCT_T bk2;
        bk2._hv2 = hv2;
        bk2._vec_treeidx.push_back(treeIdx);
        if (w_option)
            bk2._vec_dist.push_back(dist);
        _hashtab2[hv1].push_back(bk2);
    }
}

void HashRFMap::hashing_bs_without_type2_nbits_check(
        unsigned treeIdx,
        unsigned numTaxa,
        unsigned long hv1,
        unsigned long hv2,
        float dist,
        bool w_option,
        vector < vector < float > > &sim) {
        	
    ////////////////////////////////////////////////////////////////////
    // double linked list
    ////////////////////////////////////////////////////////////////////
    // 1. search hv1 in the hashtab2
    // 2. if hv1 exists
    //       search hv2
    //       if hv2 exists
    //	   insert treeIdx at the index of the linked list
    //       else
    //          insert {hv2, treeIdx} at the end of the linked list
    //     else
    //				make new list with {hv2, treeIdx} 
    //        and insert the list at HT[hv1]
    ////////////////////////////////////////////////////////////////////
    unsigned sizeVec = _hashtab2[hv1].size();
    if (sizeVec > 0) {
        for (unsigned i = 0; i < sizeVec; ++i) {
            if (_hashtab2[hv1][i]._hv2 == hv2) {
                for (unsigned j = 0; j < _hashtab2[hv1][i]._vec_treeidx.size(); ++j) {
                    sim[treeIdx][_hashtab2[hv1][i]._vec_treeidx[j]] += 1;
                }
            }
        }
    }
}

void HashRFMap::hashrfmap_clear() {
    _hashtab2.clear();
}

void HashRFMap::uhashfunc_init(
        unsigned long t,
        unsigned long n,
        float r,
        unsigned long c) {
    _HF.UHashfunc_init(t, n, r, c);
}

void HashRFMap::uhashfunc_init(
        unsigned long t,
        unsigned long n,
        float r,
        unsigned long c, int32 newseed) {
    _HF.UHashfunc_init(t, n, r, c, newseed);
}

void HashRFMap::print_hashtable() {
    for (unsigned hti = 0; hti < _hashtab2.size(); ++hti)
        if (_hashtab2[hti].size() > 0) {
            for (unsigned i = 0; i < _hashtab2[hti].size(); ++i) {
                cout << _hashtab2[hti][i]._hv2 << " : \t";
                for (unsigned k = 0; k < _hashtab2[hti][i]._vec_treeidx.size(); ++k)
                    cout << _hashtab2[hti][i]._vec_treeidx[k] << " ";
                cout << endl;
            }
        }
}
