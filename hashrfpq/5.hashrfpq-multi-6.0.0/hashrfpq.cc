///*****************************************************/
/*
    HashRF(p,q) v6.0.1 for SysBio (using two input files)
    
    <<<<<<<<<<<<<< 2007.10.19 >>>>>>>>>>>>>>>
    
    Fast algorithm to compute Robinson_Foulds topological distance between 
    phylogenetic trees based on universal hashing.
  
  AUTHOR:
    Copyright (C) 2006, 2007 Seung-Jin Sul
      Dept. of Computer Science 
      Texas A&M University
      U.S.A.
    (contact: sulsj@cs.tamu.edu)

  DESCRIPTION: 
    1. Read the first Newick tree and parsing it to tree data structure.
    2. Collect taxon names.
    3. Read all the trees and parsing them.
    4. Collect bipartitions and hash them.
    5. Compute RF distance.
    
  HISTORY:      
    11/18/2006  Start to code new version
    11/19/2006  To read tree file and to parse trees, libcov library is used
    11/21/2006  Draw problem
    11/22/2006  First tree read problem == libcov covIO::ReadTree()
    
    11/30/2006  *CRITICAL MEM MGNT: dfs_traverse ==> delete bs
    
    
    12/01/2006  Add the constants, HASHTABLE_FACTOR and C
    12/02/2006  NUM_TREES and NUM_TAXA ==> make global for speedup
    12/03/2006  Reimplement GetTaxaLabels() and dfs_traverse()
    12/10/2006  Allocated memory clear
    
    01/01/2007  v 2.0.0 Remove opt library.
    01/02/2007  Remove STL Hash, jsut use my hash class
    
    01/17/2007  v.3.0.1 11000 + 00111 = 11111 --> no need to hash
          numBitstr --> count the number of bipartitions for each tree and 
          check the number is less than (n-3).
    
    01/18/2007  delete tree one by one
    
    01/20/2007  std random number generator ==>  Mersenne Twister random 
          number generator 
          (http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html)
    
    02/01/2007  Print option #4 for ISMB07
    
    02/08/2007  After hashing a bs, the stack_bs should be cleared.
    02/08/2007  Reroot -> correct RF distance value
    
    03/17/2007  Incorporate the methods from libcov
    03/19/2007  Fix print option 1 & 2
    
    03/19/2007  Add the 64-bit representation scheme of PGM to Fast Hash-RF.
    03/19/2007  dfs_traverse_pgm() is implemented.
    
    03/22/2007  Some data files could not be read in Hash-RF.
    03/23/2007  Add myReadTree to covIO.cpp covIO.h
          To use covTree* instead of vector<covTree*>
          To debug corrupted double-linked list error
          
    03/26/2007  Four types of operations
          1. Without TYPE-III checking and n-bits representation  (Fast Hash-RF)
          2. Without TYPE-III checking and 64-bits representation (PGM+Fast Hash-RF)
          3. With    TYPE-III checking and n-bits representation  (Hash-RF)
          4. With    TYPE-III checking and 64-bits representation (PGM+Hash-RF)
          
          
    09.11.2007 Hash function init
          64bit --> map_hashrf.uhashfunc_init(NUM_TREES, BITS, HASHTABLE_FACTOR, C);
                                                         ----
          
    09.11.2007 Hash function
          hv.hv1 += _a1[i]*ii;
          hv.hv2 += _a2[i]*ii;    
          ==>
          hv.hv1 += _a1[i];
          hv.hv2 += _a2[i];   
          
          
          
    09.07.2007  64-bit optimization ????????????????????????????????????
          uint64_t ==> stack<uint64_t>
          vec_bucket = iter_hashcs->second; and vec_bucket[i]
          ===> (iter_hashcs->second).size(); // remove copy         
          
          
          
    09.25.2007 Multifurcating -> dissimilarity matrix ==> RF matrix
    
    10.18.2007 TCLAP argument processing
    
    
    
    10.22.2007 RF matrix init
          vector< vector<int> > DISSIM(NUM_TREES, vector<int>(NUM_TREES, NUM_TAXA-3));
    
    10.27.2007 Wrong RF values in 64bit modes.
          ==> 64bit --> map_hashrf.uhashfunc_init(NUM_TREES, BITS, HASHTABLE_FACTOR, C);
          ==> DO NOT USE 
          
    10.30.2007 OPTIMIZATION
          Simple vector
          Implicit BP     
          
    10.31.2007
          covTree <= DeleteAllNodes()
          Remove stacking
    
  
    
    11.12.2007 DEBUG
          In hashfunc.cc --> top2 = c*t*n;
          
          
    11.13.2007 data type
          int --> unsigned
          unsigned long
          
    11.14.2007 DEBUG
          DISSIM += ===> SIM -=
          
    11.28.2007 OPTIMIZATION
                        Without bs.count_ones_zeros();
                        No checking for nontrivial bipartition for implicit bipartition
    
    11.29.2007 OPTIMIZATION
                        double linked list to optimize distance matrix computation.
    			
    			
    12.11.2007 Newick parser
    
    12.17.2007 Unweighted + weighted
    
    
    03.05.2008 Take two input files
    
    04.11.2008 Two input files version COMPLETED!
    
      
  TO check:   
    correct # of full bucket count
    remove KeyMap

        06.16.2009 - Suzanne attempts to modify code to accept new random library     

        08.06.2009 HashRF(p,q) for supporting multifurcating trees.
        					 COMPLETED!!
        					 
        					 
        					 
     
 */
/*****************************************************/

#include <sys/time.h>
#include <sys/resource.h>
#include <cassert>
#include <valarray>
#include <fstream>
#include <iostream>
#include <string.h>

#include "label-map.hh"
#include "bitset.hh"
#include "hashfunc.hh"
#include "hash.hh"
#include "./tclap/CmdLine.h"

// For newick parser
extern "C" {
#include <newick.h>
}

using namespace std;

// Set a random number for m1 (= Initial size of hash table) 
// m1 is the closest value to (t*n)+(t*n*HASHTABLE_FACTOR)
#define HASHTABLE_FACTOR    							0.2

// the c value of m2 > c*t*n in the paper 
static unsigned long long C = 1000;
static unsigned NUM_TREES = 0; // number of trees  for 1st tree file
static unsigned NUM_TREES2 = 0; // number of trees2 for 2nd tree file
static unsigned NUM_TAXA = 0; // number of taxa
static bool WEIGHTED = false; // unweighted
static unsigned PRINT_OPTIONS = 3; // matrix
static int32 NEWSEED = 0; //for storing user specified seed
static unsigned LABEL_WIDTH = 1;
static bool MULTI = false; // multifurcating trees

//#define TIMECHECK
//#ifdef TIMECHECK
// Compute the elapsed time
#define mytimesub(result, a, b) \
do { \
(result)->tv_sec = (a)->tv_sec - (b)->tv_sec; \
(result)->tv_usec = (a)->tv_usec - (b)->tv_usec; \
if ((result)->tv_usec < 0) { \
--(result)->tv_sec; \
(result)->tv_usec += 1000000; \
} \
} while (0)
//#endif

void
GetTaxaLabels2(
        NEWICKNODE *node,
        LabelMap &lm) {
    if (node->Nchildren == 0) {
        string temp(node->label);
        lm.push(temp);
    } else {
        for (int i = 0; i < node->Nchildren; ++i) {
            GetTaxaLabels2(node->child[i], lm);
        }
    }
}

int
dfs_hashing(
        NEWICKNODE* startNode,
        LabelMap &lm,
        HashRFMap &vvec_hashrf,
        unsigned treeIdx,
        unsigned &numBitstr,
        unsigned long m1,
        unsigned long m2,
        unsigned long total_a1,
        unsigned long total_a2) {
    // If the node is leaf node, just set the place of the taxon name in the bit string to '1'
    if (startNode->Nchildren == 0) { // leaf node

        string temp(startNode->label);
        unsigned idx = lm[temp];

        // Implicit BPs /////////////////////////
        // Set the hash values for each leaf node.
        startNode->hv1 = vvec_hashrf._HF.getA1(idx);
        startNode->hv2 = vvec_hashrf._HF.getA2(idx);

        // WHY? = to collect bitstring representing the leaf node.
        return 1;
    } else {

				unsigned num_bit = 0; // to collect "1"s returned from dfs_hashrf_nbits_wo_T2_NEWICK

        for (int i = 0; i < startNode->Nchildren; ++i) {
            num_bit += dfs_hashing(startNode->child[i], lm,
                    vvec_hashrf, treeIdx, numBitstr, m1, m2, total_a1, total_a2);
        }

        // WHY? = to filter trivial BPs
        if (num_bit > 1 and num_bit < NUM_TAXA - 1) { // if non-trivial BP
            // For weighted RF
            float dist = 0.0;

            if (WEIGHTED)
                dist = startNode->weight;
                // dist = 1; // for debug
            else
                dist = 1;

            unsigned long temphv1 = 0;
            unsigned long temphv2 = 0;
            unsigned long temphv1c = 0;
            unsigned long temphv2c = 0;

            for (int i = 0; i < startNode->Nchildren; ++i) {
                temphv1 += startNode->child[i]->hv1;
                temphv2 += startNode->child[i]->hv2;
            }

            startNode->hv1 = temphv1 % m1;
            startNode->hv2 = temphv2 % m2;

            // WHY? = to check if the compliment bitstring of a bitstring for a BP exists.
            // ex) 11100 vs. 00011
            temphv1c = (total_a1 - temphv1) % m1;
            temphv2c = (total_a2 - temphv2) % m2;

            // WHY? = to check the hash valeus for compliment bipartittions.
            // ex) 11100 vs. 00011
            if (!vvec_hashrf.exist(startNode->hv1, startNode->hv2, treeIdx) &&
                    !vvec_hashrf.exist(temphv1c, temphv2c, treeIdx)) {

                ++numBitstr; // increase the number of bipartitions collected

                if (vvec_hashrf.exist(temphv1c, temphv2c)) {
                    vvec_hashrf.hashing_bs_without_type2_nbits(treeIdx, NUM_TAXA,
                            temphv1c, temphv2c, dist, WEIGHTED);
                } else {
                    vvec_hashrf.hashing_bs_without_type2_nbits(treeIdx, NUM_TAXA,
                            startNode->hv1, startNode->hv2, dist, WEIGHTED);
                }
            }
        }


        return num_bit;
    }
}

int
dfs_hashing_check(
        NEWICKNODE* startNode,
        LabelMap &lm,
        HashRFMap &vvec_hashrf,
        unsigned treeIdx,
        unsigned &numBitstr,
        unsigned long m1,
        unsigned long m2,
        unsigned long total_a1,
        unsigned long total_a2,
        vector< vector<float> > & sim) {
    // If the node is leaf node, just set the place of the taxon name in the bit string to '1'
    if (startNode->Nchildren == 0) { // leaf node

        string temp(startNode->label);
        unsigned idx = lm[temp];

        // Implicit BPs /////////////////////////
        // Set the hash values for each leaf node.
        startNode->hv1 = vvec_hashrf._HF.getA1(idx);
        startNode->hv2 = vvec_hashrf._HF.getA2(idx);

        // WHY? = to collect bitstring representing the leaf node.
        return 1;
    } else {

        unsigned num_bit = 0;

        for (int i = 0; i < startNode->Nchildren; ++i) {
            num_bit += dfs_hashing_check(startNode->child[i], lm,
                    vvec_hashrf, treeIdx, numBitstr, m1, m2, total_a1, total_a2, sim);
        }

        // WHY? = to filter trivial BPs
        if (num_bit > 1 and num_bit < NUM_TAXA - 1) { // if non-trivial BP
            // For weighted RF
            float dist = 0.0;

            if (WEIGHTED)
                dist = startNode->weight;
                //dist = 1; // debug
            else
                dist = 1;

            unsigned long temphv1 = 0;
            unsigned long temphv2 = 0;
            unsigned long temphv1c = 0;
            unsigned long temphv2c = 0;

            for (int i = 0; i < startNode->Nchildren; ++i) {
                temphv1 += startNode->child[i]->hv1;
                temphv2 += startNode->child[i]->hv2;
            }

            startNode->hv1 = temphv1 % m1;
            startNode->hv2 = temphv2 % m2;

            // WHY? = to check if the compliment bitstring of a bitstring for a BP exists.
            // ex) 11100 vs. 00011
            temphv1c = (total_a1 - temphv1) % m1;
            temphv2c = (total_a2 - temphv2) % m2;

            ++numBitstr;


            if (vvec_hashrf.exist(temphv1c, temphv2c)) {
                vvec_hashrf.hashing_bs_without_type2_nbits_check(treeIdx, NUM_TAXA,
                        temphv1c, temphv2c, dist, WEIGHTED, sim);
            } else {
                vvec_hashrf.hashing_bs_without_type2_nbits_check(treeIdx, NUM_TAXA,
                        startNode->hv1, startNode->hv2, dist, WEIGHTED, sim);
            }
        }


        return num_bit;
    }
}



static void
print_rf_matrix_multi(
        vector< vector<float> > &SIM,
        unsigned options,
        string outfile,
        vector<unsigned> &vec_numBP) {
    ofstream fout;

    if (outfile != "")
        fout.open(outfile.c_str());

    unsigned totalBPs = vec_numBP[0]; // because all the trees have the same number of max BPs

    switch (options) {
        case 0:
            return;
//        case 1:
//            if (outfile == "") {
//            		cout << "\nRobinson-Foulds distance (list format):\n";
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        if (i == j)
//                            cout << "<" << i << "," << j << "> " << 0 << endl;
//                        else {
//                            if (!WEIGHTED)
//                                cout << "<" << i << "," << j << "> " << totalBPs - (float((SIM[i][j] + SIM[j][i]) / 2)) << endl;
//                            else
//                                cout << "<" << i << "," << j << "> " << float((SIM[i][j] + SIM[j][i]) / 4) << endl;
//                        }
//                    }
//                }
//            } else {
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        if (i == j)
//                            fout << "<" << i << "," << j << "> " << 0 << endl;
//                        else {
//                            if (!WEIGHTED)
//                                fout << "<" << i << "," << j << "> " << totalBPs - (float((SIM[i][j] + SIM[j][i]) / 2)) << endl;
//                            else
//                                fout << "<" << i << "," << j << "> " << float((SIM[i][j] + SIM[j][i]) / 4) << endl;
//                        }
//                    }
//                }
//            }
//
//            break;
//        case 2:
//            cout << "\nRobinson-Foulds distance (rate):\n";
//            if (WEIGHTED) {
//                cout << "Fatal error: RF rate is only for unweighted RF distance.\n";
//                exit(0);
//            }
//
//            if (outfile == "") {
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        cout << "<" << i << "," << j << "> ";
//                        if (i == j)
//                            cout << 0 << endl;
//                        else
//                            cout << ((vec_numBP[i] - SIM[i][j])+(vec_numBP[j] - SIM[j][i])) / 2 / (NUM_TAXA - 3) * 100 << endl;
//                    }
//                }
//            } else {
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        fout << "<" << i << "," << j << "> ";
//                        if (i == j)
//                            fout << 0 << endl;
//                        else
//                            fout << ((vec_numBP[i] - SIM[i][j])+(vec_numBP[j] - SIM[j][i])) / 2 / (NUM_TAXA - 3) * 100 << endl;
//                    }
//                }
//            }
//            break;
        case 3:            
            if (outfile == "") {
            		cout << "\nRobinson-Foulds distance (matrix format):\n";
                for (unsigned i = 0; i < NUM_TREES; ++i) {
                    for (unsigned j = 0; j < NUM_TREES; ++j) {
                        if (i == j)
                            cout << setw(LABEL_WIDTH) << "0" << ' ';
                        else
                            if (WEIGHTED) {
                            cout << setw(LABEL_WIDTH) << ((vec_numBP[i] - SIM[i][j])+(vec_numBP[j] - SIM[j][i])) / 2 << ' ';
                        } else {
                            cout << setw(LABEL_WIDTH) << ((vec_numBP[i] - SIM[i][j])+(vec_numBP[j] - SIM[j][i])) / 2 << ' ';
                        }
                    }
                    cout << endl;
                }
                cout << endl;
            } else {
                for (unsigned i = 0; i < NUM_TREES; ++i) {
                    for (unsigned j = 0; j < NUM_TREES; ++j) {
                        if (i == j)
                            fout << setw(LABEL_WIDTH) << "0" << ' ';
                        else
                            if (WEIGHTED)
                            fout << setw(LABEL_WIDTH) << ((vec_numBP[i] - SIM[i][j])+(vec_numBP[j] - SIM[j][i])) / 2 << ' ';
                        else {
                            fout << setw(LABEL_WIDTH) << ((vec_numBP[i] - SIM[i][j])+(vec_numBP[j] - SIM[j][i])) / 2 << ' ';
                        }
                    }
                    fout << endl;
                }
                fout << endl;
            }
            break;
    }

    if (outfile != "")
        fout.close();
}

static void
print_rf_matrix(
        vector< vector<float> > &SIM,
        unsigned options,
        string outfile,
        vector<unsigned> &vec_numBP) {
    ofstream fout;
    if (outfile != "") {
        fout.open(outfile.c_str());
    }

    unsigned totalBPs = vec_numBP[0]; // because all the trees have the same number of max BPs

    switch (options) {
        case 0:
            return;
//        case 1:
//            cout << "\nRobinson-Foulds distance (list format):\n";
//
//            if (outfile == "") {
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        if (i == j)
//                            cout << "<" << i << "," << j << "> " << 0 << endl;
//                        else {
//                            if (!WEIGHTED)
//                                cout << "<" << i << "," << j << "> " << totalBPs - (float((SIM[i][j] + SIM[j][i]) / 2)) << endl;
//                            else
//                                cout << "<" << i << "," << j << "> " << float((SIM[i][j] + SIM[j][i]) / 4) << endl;
//                        }
//                    }
//                }
//            } else {
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        if (i == j)
//                            fout << "<" << i << "," << j << "> " << 0 << endl;
//                        else {
//                            if (!WEIGHTED)
//                                fout << "<" << i << "," << j << "> " << totalBPs - (float((SIM[i][j] + SIM[j][i]) / 2)) << endl;
//                            else
//                                fout << "<" << i << "," << j << "> " << float((SIM[i][j] + SIM[j][i]) / 4) << endl;
//                        }
//                    }
//                }
//            }
//
//            break;
//        case 2:
//            cout << "\nRobinson-Foulds distance (rate):\n";
//            if (WEIGHTED) {
//                cout << "Fatal error: RF rate is only for unweighted RF distance.\n";
//                exit(0);
//            }
//
//            if (outfile == "") {
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        cout << "<" << i << "," << j << "> ";
//                        if (i == j)
//                            cout << 0 << endl;
//                        else
//                            cout << (float) (totalBPs - ((SIM[i][j] + SIM[j][i]) / 2)) / (NUM_TAXA - 3) * 100 << endl;
//                    }
//                }
//            } else {
//                for (unsigned i = 0; i < NUM_TREES; ++i) {
//                    for (unsigned j = 0; j < NUM_TREES; ++j) {
//                        fout << "<" << i << "," << j << "> ";
//                        if (i == j)
//                            fout << 0 << endl;
//                        else
//                            fout << (float) (totalBPs - ((SIM[i][j] + SIM[j][i]) / 2)) / (NUM_TAXA - 3) * 100 << endl;
//                    }
//                }
//            }
//            break;
        case 3:
            cout << "\nRobinson-Foulds distance (matrix format):\n";
            if (outfile == "") {
                for (unsigned i = 0; i < NUM_TREES; ++i) {
                    for (unsigned j = 0; j < NUM_TREES; ++j) {
                        if (i == j)
                            cout << setw(LABEL_WIDTH) << "0" << ' ';
                        else
                            if (WEIGHTED)
                            cout << setw(LABEL_WIDTH) << float((SIM[i][j] + SIM[j][i]) / 4) << ' ';
                        else {
                            cout << setw(LABEL_WIDTH) << totalBPs - (float((SIM[i][j] + SIM[j][i]) / 2)) << ' ';
                            //	           		cout << setw(LABEL_WIDTH) << (float((SIM[i][j] + SIM[j][i])/2)) << ' ';
                        }
                    }
                    cout << endl;
                }
                cout << endl;
            } else {
                for (unsigned i = 0; i < NUM_TREES; ++i) {
                    for (unsigned j = 0; j < NUM_TREES; ++j) {
                        if (i == j)
                            fout << setw(LABEL_WIDTH) << "0" << ' ';
                        else
                            if (WEIGHTED)
                            fout << setw(LABEL_WIDTH) << float((SIM[i][j] + SIM[j][i]) / 4) << ' ';
                        else
                            fout << setw(LABEL_WIDTH) << totalBPs - (float((SIM[i][j] + SIM[j][i]) / 2)) << ' ';
                    }
                    fout << endl;
                }
                fout << endl;
            }
            break;
    }

    if (outfile != "")
        fout.close();
}


int main(int argc, char** argv) {
    string outfilename;
    string infilename;
    char* fname1 = NULL;
    char* fname2 = NULL;
    unsigned TOTAL_NUMTREES = 0;

    // TCLAP
    try {

        // Define the command line object.
        string helpMsg = "HashRF\n";

        helpMsg += "Input file: \n";
        helpMsg += "   The current version of HashRF only supports the Newick format.\n";

        helpMsg += "Example of Newick tree: \n";
        helpMsg += "   (('Chimp':0.052625,'Human':0.042375):0.007875,'Gorilla':0.060125,\n";
        helpMsg += "   ('Gibbon':0.124833,'Orangutan':0.0971667):0.038875);\n";
        helpMsg += "   ('Chimp':0.052625,('Human':0.042375,'Gorilla':0.060125):0.007875,\n";
        helpMsg += "   ('Gibbon':0.124833,'Orangutan':0.0971667):0.038875);\n";

        helpMsg += "Print out mode: (defualt = matrix).\n";
        helpMsg += "   -p no, no output (default).\n";
        helpMsg += "   -p list, print RF distance in list format.\n";
        helpMsg += "   -p rate, print RF distance rate in list format.\n";
        helpMsg += "   -p matrix, print reuslting distance in matrix format.\n";

        helpMsg += "File option: \n";
        helpMsg += "   -o <export-file-name>, save the distance result in a file.\n";

        helpMsg += "Weighted RF distance: to select RF distance mode between weighted and unweighted (defualt = unweighted).\n";
        helpMsg += "   -w, compute weighted RF distance.\n";

        helpMsg += "Examples: \n";
        helpMsg += "  hashrf foo1.tre foo2.tre 1000 1000\n";
        helpMsg += "  hashrf bar1.tre bar2.tre 1000 1000 -w\n";
        helpMsg += "  hashrf bar1.tre bar2.tre 1000 1000 -w -p matrix\n";
        helpMsg += "  hashrf bar1.tre bar2.tre 1000 1000 -w -p list\n";
        helpMsg += "  hashrf bar1.tre foo1.tre 1000 1000 -w -p list -o output.dat\n";

        TCLAP::CmdLine cmd(helpMsg, ' ', "0.9");

        TCLAP::UnlabeledValueArg<string> fnameArg("name", "1st file name", true, "intree", "1st input tree file name");
        cmd.add(fnameArg);

        TCLAP::UnlabeledValueArg<string> fnameArg2("name2", "2nd file name", true, "intree2", "2nd input tree file name");
        cmd.add(fnameArg2);

        TCLAP::UnlabeledValueArg<int> numtreeArg("numtree", "number of trees", true, 2, "Number of trees in the 1st tree file");
        cmd.add(numtreeArg);

        TCLAP::UnlabeledValueArg<int> numtreeArg2("numtree2", "number of trees2", true, 2, "Number of trees in the 2nd tree file");
        cmd.add(numtreeArg2);

        TCLAP::SwitchArg weightedSwitch("w", "weighted", "Compute weighted RF distance", false);
        cmd.add(weightedSwitch);

        TCLAP::ValueArg<string> printArg("p", "printoptions", "print options", false, "matrix", "Print options");
        cmd.add(printArg);

        TCLAP::ValueArg<int> cArg("c", "cvalue", "c value", false, 1000, "c value");
        cmd.add(cArg);

        TCLAP::ValueArg<int> seedArg("s", "seedvalue", "user specified seed value", false, 1000, "user specified seed value");
        cmd.add(seedArg);

        TCLAP::ValueArg<string> outfileArg("o", "outfile", "Output file name", false, "", "Output file name");

        // for multi
        TCLAP::SwitchArg multiSwitch("m", "multifurcating", "For multifurcating trees", false);
        cmd.add(multiSwitch);

        cmd.add(outfileArg);

        cmd.parse(argc, argv);

        NUM_TREES = numtreeArg.getValue();
        NUM_TREES2 = numtreeArg2.getValue();
        TOTAL_NUMTREES = NUM_TREES + NUM_TREES2;

        fname1 = new char[fnameArg.getValue().length() + 1];
        fname2 = new char[fnameArg2.getValue().length() + 1];

        strcpy(fname1, fnameArg.getValue().c_str());
        strcpy(fname2, fnameArg2.getValue().c_str());

        if (NUM_TREES == 0) { //if number of trees in first file is unknown
            string strFileLine;
            unsigned long ulLineCount;
            ulLineCount = 0;

            ifstream infile(fname1);
            
            //count the number of lines in the file
            if (infile) {
                while (getline(infile, strFileLine)) {
                    ulLineCount++;
                }
            } else {
                cout << "Fatal error: 1st file open error\n";
                exit(0);
            }

            cout << "*** Number of trees in the 1st input file: " << ulLineCount << endl;
            NUM_TREES = ulLineCount;

            infile.close();
        }

        if (NUM_TREES2 == 0) { //if number of trees in second file is unknown
            string strFileLine;
            unsigned long ulLineCount;
            ulLineCount = 0;

            ifstream infile(fname2);
            
            //count the number of lines in file
            if (infile) {
                while (getline(infile, strFileLine)) {
                    ulLineCount++;
                }
            } else {
                cout << "Fatal error: 2nd file open error\n";
                exit(0);
            }

            cout << "*** Number of trees in the 2nd input file: " << ulLineCount << endl;
            NUM_TREES2 = ulLineCount;
            infile.close();
        }

        if (NUM_TREES < 1 || NUM_TREES2 < 1) { //if we don't have at least two trees
            cerr << "Fatal error: at least two trees expected.\n";
            exit(2);
        }

        if (weightedSwitch.getValue())
            WEIGHTED = true;

        if (printArg.getValue() != "matrix") {
            string printOption = printArg.getValue();
            if (printOption == "no")
                PRINT_OPTIONS = 0;
            if (printOption == "list")
                PRINT_OPTIONS = 1;
            if (printOption == "rate")
                PRINT_OPTIONS = 2;
        }

        if (cArg.getValue())
            C = cArg.getValue();

        if (multiSwitch.getValue())
            MULTI = true;

        if (seedArg.getValue()) {
            NEWSEED = seedArg.getValue();
        }
        outfilename = outfileArg.getValue();
        cout << "NEWSEED is: " << NEWSEED << endl;
    } catch (TCLAP::ArgException &e) { // catch any exceptions
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    }

    //grabbing initial newick tree for processing
    NEWICKTREE *newickTree;
    int err;
    FILE *fp;

    fp = fopen(fname1, "r");
    if (!fp) {
        cout << "Fatal error: file open error\n";
        exit(0);
    }

    newickTree = loadnewicktree2(fp, &err);

    if (!newickTree) {
        switch (err) {
            case -1 : printf("Out of memory\n");
                break;
            case -2 : printf("parse error\n");
                break;
            case -3 : printf("Can't load file\n");
                break;
            default:
                printf("Error %d\n", err);
        }
    }


    /*****************************************************/
    cout << "\n*** Collecting the taxon labels ***\n";
    /*****************************************************/
    LabelMap lm;

    try {
        GetTaxaLabels2(newickTree->root, lm);
    } catch (LabelMap::AlreadyPushedEx ex) {
        cerr << "Fatal error: The label '" << ex.label << "' appeard twice in " << endl;
        exit(2);
    }
    NUM_TAXA = lm.size();
    cout << "    Number of taxa = " << lm.size() << endl;
    killnewicktree(newickTree);
    fclose(fp);


    /*****************************************************/
    cout << "\n*** Reading tree file and collecting bipartitions ***\n";
    /*****************************************************/
    HashRFMap vvec_hashrf; // Class HashRFMap

    // for multi
    vector<unsigned> vec_numBP; // To collect the number of bipartitions of each tree.
    vec_numBP.resize(NUM_TREES);

    ////////////////////////////
    // Init hash function class
    ////////////////////////////
    // HashRFMap vvec_hashrf; // Class HashRFMap
    unsigned long M1 = 0;
    unsigned long M2 = 0;

    // for multi
    unsigned long total_A1 = 0;
    unsigned long total_A2 = 0;

    // initialize the hash function
    if (NEWSEED != 0) {
        vvec_hashrf.uhashfunc_init(NUM_TREES2, NUM_TAXA, HASHTABLE_FACTOR, C, NEWSEED);
    } else {
        vvec_hashrf.uhashfunc_init(NUM_TREES2, NUM_TAXA, HASHTABLE_FACTOR, C);
    }
    M1 = vvec_hashrf._HF.getM1();
    M2 = vvec_hashrf._HF.getM2();

    // Compute the sum of A1 and A2 sets of random intergers
    // WHY? = to compute two hsah values implicitly for compliment 
    // bipartitions.
    // ex) 11000 vs 00111
    for (unsigned i = 0; i < NUM_TAXA; ++i) {
        total_A1 += vvec_hashrf._HF.getA1(i);
        total_A2 += vvec_hashrf._HF.getA2(i);
    }
    vvec_hashrf._hashtab2.resize(M1);

    // for multi
    vector< vector<float> > SIM(NUM_TREES, vector<float>(NUM_TREES, 0.0)); // similarity matrix

    FILE *fp2;
    fp2 = fopen(fname2, "r");
    if (!fp2) {
        cout << "Fatal error: 2nd file open error\n";
        exit(0);
    }

    cout << "    Number of trees (1st) = " << NUM_TREES << endl;
    cout << "    Number of trees (2nd) = " << NUM_TREES2 << endl;
    cout << endl;

    /**************************************************************/
    cout << "\n*** Compute distance ***\n";
    /**************************************************************/

    for (unsigned treeIdx2 = 0; treeIdx2 < NUM_TREES2; ++treeIdx2) {
        NEWICKTREE *newickTree2;
        newickTree2 = loadnewicktree2(fp2, &err);

        if (!newickTree2) {
            switch (err) {
                case -1 : printf("Out of memory\n");
                    break;
                case -2 : printf("parse error\n");
                    break;
                case -3 : printf("Can't load file\n");
                    break;
                default: printf("Error %d\n", err);
            }
        } else {
            unsigned numBitstr = 0;
            
            // for multi
            dfs_hashing(newickTree2->root, lm, vvec_hashrf, treeIdx2, numBitstr,
                    M1, M2, total_A1, total_A2);

            killnewicktree(newickTree2);
        }
    }
    fclose(fp2);
    
    // for debug
//		vvec_hashrf.print_hashtable();  

    fp = fopen(fname1, "r");
    if (!fp) {
        cout << "Fatal error: 1st file open error\n";
        exit(0);
    }

    for (unsigned treeIdx = 0; treeIdx < NUM_TREES; ++treeIdx) {
        NEWICKTREE *newickTree1;
        newickTree1 = loadnewicktree2(fp, &err);
        if (!newickTree1) {
            switch (err) {
                case -1 : printf("Out of memory\n");
                    break;
                case -2 : printf("parse error\n");
                    break;
                case -3 : printf("Can't load file\n");
                    break;
                default: printf("Error %d\n", err);
            }
        } else {
            unsigned numBitstr = 0; // to store # of bipartitions

            // for multi
            dfs_hashing_check(newickTree1->root, lm, vvec_hashrf, treeIdx,
                    numBitstr, M1, M2, total_A1, total_A2, SIM);

            // for multi
            vec_numBP[treeIdx] = numBitstr;

            killnewicktree(newickTree1);
        }
    }
    fclose(fp);


    ////////////////////////////
    //	int uBID = 0;
    //  int avgChainLength = 0;
    //	int count = 0;
    //	unsigned int scCount = 0;
    //	unsigned int mjCount = 0;
    ////////////////////////////

    if (MULTI)
        print_rf_matrix_multi(SIM, 3, outfilename, vec_numBP); // only matrix format can work
    else
        print_rf_matrix(SIM, 3, outfilename, vec_numBP);



#ifdef TIMECHECK
    printf("\n\n");
    printf("Label collect:          %lds and %ldus\n", echodelay1.tv_sec, echodelay1.tv_usec);
    printf("Assign random integer:  %lds and %ldus\n", echodelay2.tv_sec, echodelay2.tv_usec);
    printf("Bipartition collect:    %lds and %ldus\n", echodelay3.tv_sec, echodelay3.tv_usec);
    printf("Compute distance:       %lds and %ldus\n", echodelay4.tv_sec, echodelay4.tv_usec);
    //printf("Construct SC:         %lds and %ldus\n", echodelay5.tv_sec, echodelay5.tv_usec);
    //printf("Make newick:          %lds and %ldus\n", echodelay6.tv_sec, echodelay6.tv_usec);
#endif



    /*****************************************************/
    //  cout << "\n*** Print statistics ***\n";
    /*****************************************************/
    // CPU time comsumed
    struct rusage a;
    if (getrusage(RUSAGE_SELF, &a) == -1) {
        cerr << "Fatal error: getrusage failed.\n";
        exit(2);
    }
    cout << "\n    Total CPU time: " << a.ru_utime.tv_sec + a.ru_stime.tv_sec << " sec and ";
    cout << a.ru_utime.tv_usec + a.ru_stime.tv_usec << " usec.\n";

    // Resolution of strict consensus tree
    //  cout << "    Number of full buckets: "
    //     << numFullBucket << endl;
    //  cout << "    Resolution of strict consensus tree: "
    //     << double(numFullBucket) / double(NUM_TAXA-3) << endl;
    //  cout << "    Size of the Hash table: "
    //     << sizeHashtab << endl;
    //  cout << "    Number of non-shared bipartitions (rate): "
    //     << numNotSharedBS << " (" << double(numNotSharedBS)/double(sizeHashtab)
    //     << ")" << endl;


    /*****************************************************/
    //  cout << "\n*** Clear allocated memory***\n";
    /*****************************************************/
    SIM.clear();
    vvec_hashrf.hashrfmap_clear();
    vec_numBP.clear();




    return 0; //codes >0 are error codes!
}





