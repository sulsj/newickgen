///*****************************************************/
//
// Copyright (C) 2006, 2007 Seung-Jin Sul
// 		Department of Computer Science
// 		Texas A&M University
// 		Contact: sulsj@cs.tamu.edu
//
// 		CLASS DEFINITION
//		CHashFunc: Universal hash functions
//
///*****************************************************/

#ifndef HASHFUNC_HH
#define HASHFUNC_HH

#include <iostream>
#include "bitset.hh"
//#include "randomc.h" // MT random number generator


typedef struct {
	unsigned long hv1;
	unsigned long hv2;
} HV_STRUCT_T;	

typedef signed long int32;
typedef unsigned long uint32;

class CHashFunc {
	
	unsigned long  _m1;  // prime number1 for hash function1
	unsigned long  _m2;  // prime number1 for hash function2
	unsigned long   _t;  // number of trees
	unsigned long   _n;  // number of taxa
	unsigned long* _a1;  // random numbers for hash function1
	unsigned long* _a2;  // random numbers for hash function2
	float          _r;  // hash table factor
	unsigned long  _c;  // double collision factor: constant for c*t*n of hash function2;
	
public:
  CHashFunc() : _m1(0), _m2(0), _t(0), _n(0), _a1(NULL), _a2(NULL), _r(0.0), _c(0) {}
  CHashFunc(unsigned long t, unsigned long n, float r, unsigned long c);
  ~CHashFunc();
  
  void UHashfunc_init(unsigned long t, unsigned long n, float r, unsigned long c);
  void UHashfunc_init(unsigned long t, unsigned long n, float r, unsigned long c, int32 newseed);
    
//	unsigned long UHashFunc1(const std::string &c);
//	unsigned long UHashFunc2(const std::string &c);	
//	unsigned long UHashFunc1(const BitSet &bs);
//	unsigned long UHashFunc2(const BitSet &bs);
	
//	void UHashFunc(HV_STRUCT_T &hv, const std::string &c, int num_taxa);
  void UHashFunc(HV_STRUCT_T &hv, BitSet &bs, unsigned num_taxa);
//	void UHashFunc(HV_STRUCT_T &hv, const std::string &c, int num_taxa); // with type3

////	void UHashFunc(HV_STRUCT_T &hv, BitSet &bs); // for pgm	
  void UHashFunc(HV_STRUCT_T &hv, uint64_t bs64, unsigned numBits); 
  
  unsigned long long GetPrime(unsigned long top_num);
  unsigned long long GetPrime(unsigned long long top_num, unsigned from);
  
  // Implicit bp
  unsigned long getA1(unsigned idx) { return (_a1[idx]); }
  unsigned long getA2(unsigned idx) { return (_a2[idx]); }
  unsigned long getM1() { return _m1; }
  unsigned long getM2() { return _m2; }
  
};

#endif
