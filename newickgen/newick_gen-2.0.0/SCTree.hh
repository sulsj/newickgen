#ifndef __SCTree_hh__
#define __SCTree_hh__

#include  "SCNode.hh"

class SCTree {
public:
    SCTree();
    SCTree(string name);
    ~SCTree();

    SCNode*         root;
    vector<SCNode*> nodeList;
    map<string, unsigned int> parentList;

    void            DrawOnTerminal(bool distances = false);
    void            PrintParentlist();
    unsigned int    FindParent2(string name);

    //string          GetTreeString(bool distances = false, double scaleFactor = 100.0, int seed = 0);
    //void            GetTreeRecurse(string& ret, SCNode* node, bool distances, double scaleFactor, int seed = 0);
    string          GetTreeString(bool distances = false, double scaleFactor = 100.0);
    void            GetTreeRecurse(string& ret, SCNode* node, bool distances, double scaleFactor);
    SCNode*         GetLeastSubtree(SCNode *node);
    void            DeleteAllNodes();
};





#endif


