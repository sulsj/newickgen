/**********************************************************************/
/*
    newickgen

    Load a tree and gen the identical but different Newick string


    Revisions:

    09.03.2009 DEBUG: for 64-bit machines
            PROBLEM: same taxon names exists more than one time in the resulting Newick string

    09.07.2009 Using STL bitset

    11.06.2009 UPDATE
            Collect n-3 biparitition.
            Update to deal with Grant's trees.

    KEY ROUTINE
        * SCTree.cc: RANDOM NUMBER GENERATED WHEN SELECTING ONE OF CHILDREN
          NODES TO VISIT. THIS WORKS FOR GENERATING DIFFERENT NEWICK
          STRINGS.

    8.6.2010
        * Add include branch length option
        * No need for using hash table to generate trees. Get rid of all
          realted things.
        * Add blTable to store branch length

    8.13.2010 Reimplement 3.0.0

*/
/***********************************************************************/

// My classes
//#include "SCTree.hh"
//#include "SCNode.hh"
//#include <fstream>
#include <iostream>
#include <fstream>

// From Split-Dist
//#include "label-map.hh"

// Etc
#include <cassert>
#include <sys/time.h>
#include <sys/resource.h>

// For newick parser
extern "C" {
#include <newick.h>
}

// For command line arguments
#include <./tclap/CmdLine.h>
//#include <bitset>
#include <stdlib.h>

#include "RandomLib/Random.hpp"
#include <string.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <map>


using namespace std;

// if double collision is reported, increase this!
// the c value of m2 > c*t*n in the paper
//static unsigned int     C           = 1000;
//static unsigned int     NUMTAXA     = 0;        // Number of taxa
//static unsigned long    NEWSEED     = 0;        // for storing user specified seed value
bool                    bBRANCHLEN  = false;

//string itos(int i)  // convert int to string
//{
//stringstream s;
//s << i;
//return s.str();
//}

//void
//GetTaxaLabels2(
//NEWICKNODE *node,
//LabelMap &lm)
//{
//if (node->Nchildren == 0) {
//string temp(node->label);
//lm.push(temp);
//} else
//for (unsigned i = 0; i < node->Nchildren; ++i)
//GetTaxaLabels2(node->child[i], lm);
//}

void labelInternalNodes(NEWICKNODE* startNode, int &num)
{
    if (startNode->Nchildren == 0) { // leaf node
        assert(startNode->label != NULL);
        return;
    }
    else {
        if (startNode->Nchildren > 2) {
            cout << "    Input tree = non-binary\n";
        }
        for (unsigned i = 0; i < startNode->Nchildren; ++i) {
            labelInternalNodes(startNode->child[i], num);
        }
        if (startNode->label == NULL) {
            char buf[32];
            sprintf(buf, "int%d", num);
            startNode->label = (char*) malloc(strlen(buf) + 1);
            strcpy(startNode->label, buf);
        }
        num++;
    }
}

//bitset<BITSETSZ> *
//DFS(
//NEWICKNODE* startNode,
//LabelMap &lm,
//vector<bitset<BITSETSZ> *> & vecBs,
//unsigned &numBPCollected,
//map<string, double> &blTable)
//{
//// If the node is leaf node, just set the place of the taxon name
//// in the bit string to '1' and push the bit string into stack
//if (startNode->Nchildren == 0) { // leaf node

//// Get the location of taxon label in lm
//// which is used to set "1" in bitstring.
//string temp(startNode->label);
////cout << temp << " " << startNode->weight << "\n";
//unsigned int idx = lm[temp];
//if (bBRANCHLEN)
//blTable[temp] = startNode->weight;

//// Here we make an actual bitstring
//bitset<BITSETSZ> * bs = new bitset<BITSETSZ>;
//(*bs)[BITSETSZ-1-idx] = true;
//int numOnes = 0;
//numOnes = (*bs).count();
//if (numOnes > 1 || numOnes == 0) {
//cout << "numOnes = " << numOnes << endl;
//cerr << "ERROR: bitstring creation error\n";
//exit(0);
//}
//return bs; // return the bitstring for this leaf node (=taxon)
//}
//else { // non-leaf node
////cout << startNode->label << " " << startNode->weight << "\n";
//bitset<BITSETSZ> * ebs[startNode->Nchildren];
//assert(startNode->Nchildren >= 2);
//if (startNode->Nchildren > 2) {
//cout << "    Input tree = non-binary\n";
//}
//for (unsigned i = 0; i < startNode->Nchildren; ++i) {
//ebs[i] = DFS(startNode->child[i], lm, vecBs, numBPCollected, blTable);
//}
////string temp(startNode->label);
////cout << temp << endl;
////cout << startNode->weight << "\n";
////if (startNode->label == NULL) {
////char *name = "int";
////char *id = NULL;
////itoa(numBPCollected, id, 10);
////startNode->label = (char*)malloc(strlen(name)+1);

///*
//// For storing weights for internal nodes
//string newLabel = "int" + itos(numBPCollected);
////cout << newLabel << endl;
//startNode->label = new char[newLabel.length() + 1];
//strcpy(startNode->label, newLabel.c_str());
//blTable[newLabel] = startNode->weight;
////cout << "blTable[newLabel]=" << blTable[newLabel] << endl;
//*/
////}

//// At this point, we find a bipartition.
//// Thus, OR the bitstrings and make a bit string for the bipartition
//bitset<BITSETSZ> *bs = new bitset<BITSETSZ>;
//for (unsigned i = 0; i < startNode->Nchildren; ++i) {
//if (ebs[i]) {
//*bs |= *(ebs[i]);
//delete ebs[i];
//ebs[i] = NULL;
//} else {
//cout << "ERROR: null bitstring\n";
//exit(0);
//}
//}
//int numOnes = 0;
//numOnes = (*bs).count();
//if (numOnes < 1) {
//cout << "numOnes = " << numOnes << endl;
//cerr << "ERROR: bitstring OR error\n";
//exit(0);
//}
//++numBPCollected;
//if (numBPCollected < NUMTAXA-2) {
//bitset<BITSETSZ> *tempbs = new bitset<BITSETSZ>;
//*tempbs = *bs;
//vecBs.push_back(tempbs);
//}
//return bs;
//}
//}

NEWICKNODE* GetLeastSubtree(NEWICKNODE *node)
{
    if (node->Nchildren == 0)
        return node;
    NEWICKNODE* temp = node->child[0];
    bool allInts = true;
    unsigned int numChildren = node->Nchildren;
    for (unsigned int i = 0; i < numChildren; i++) {
        if (node->child[i]->Nchildren == 0) {
            allInts = false;
            if (!temp->Nchildren == 0 || (temp->Nchildren == 0 && strcmp(node->child[i]->label, temp->label) < 0))
                temp = node->child[i];
        }
    }
    if (allInts) {
        vector<NEWICKNODE*> kids;
        kids.resize(numChildren);
        for (unsigned int i = 0; i < numChildren; i++)
            kids[i] = GetLeastSubtree(node->child[i]);
        temp = kids[0];
        for (unsigned int i = 0; i < numChildren; i++)
            if (kids[i]->label < temp->label)
                temp = kids[i];
    }
    return temp;
}



void GetTreeRecurse(string &ret, NEWICKNODE* node, bool bDistances, float scaleFactor)
{
    string distance = "";
    if (bDistances) {
        double bl = node->weight / scaleFactor;
        // We don't want to output exponential notation
        if (bl < 0.000001)
            distance = "0.000001";
        else {
            char buf[32];
            sprintf(buf, "%f", bl);
            distance = buf;
        }
    }
    if (node->Nchildren == 0) {
        char buf[32];
        sprintf(buf, "%s", node->label);
        string nodeLabel = buf;
        if (bDistances) {
            ret.append(nodeLabel + ":" + distance);
        }
        else {
            ret.append(nodeLabel);
        }
        return;
    }
    ret += "(";
    unsigned int numChildren = node->Nchildren;
    //cout << "numChildren=" << numChildren << endl;
    if (numChildren == 2) {
        //printf("%s %s\n",  node->child[0]->label, node->child[1]->label);
        if (strcmp(node->child[0]->label, node->child[1]->label) > 0) {
            NEWICKNODE *temp = node->child[0];
            node->child[0] = node->child[1];
            node->child[1] = temp;
        }
        //if (!node->child[0]->Nchildren == 0 && !node->child[1]->Nchildren == 0) {
        //NEWICKNODE *temp, *temp2;
        ////temp = GetLeastSubtree(node);
        ////temp2 = temp->parent;
        //while (temp2 != node) {
        //temp = temp2;
        ////temp2 = temp2->parent;
        //}
        //if (temp->label == node->child[1]->label) {
        //node->child[1] = node->child[0];
        //node->child[0] = temp;
        //}
        //}
        ///////////////////////////////////////////////////////////////
        RandomLib::Random rnd;
        unsigned temp = rnd.Integer<unsigned>(2); // rand int [0, 2), or 0 or 1
        if (temp == 0) {
            NEWICKNODE *temp = node->child[0];
            node->child[0] = node->child[1];
            node->child[1] = temp;
        }
        ///////////////////////////////////////////////////////////////
    }
    //else {
    //cerr << "ERROR: numChildren > 2\n";
    //exit(2);
    //}
    for (unsigned int i = 0; i < numChildren; i++) {
        GetTreeRecurse(ret, node->child[i], bDistances, scaleFactor);
        if (i != numChildren - 1)
            ret += ",";
    }
    if (!strcmp(node->label, "root")) {
        ret += ")";
        return;
    }
    ret = ret + ")";
    if (bDistances)
        ret = ret + ":" + distance;
    else
        ret = ret;
}


string GetTreeString(
    NEWICKNODE* node,
    bool bDistances,
    double scaleFactor)
{
    string tree;
    GetTreeRecurse(tree, node, bDistances, scaleFactor);
    return tree + ";";
}


int main(int argc, char** argv)
{
    string outFileName;
    //float p = 0.0;
    // TCLAP
    try {
        // Define the command line object.
        string  helpMsg  = "newickgen\n";
        TCLAP::CmdLine cmd(helpMsg, ' ', "3.0.0");
        TCLAP::UnlabeledValueArg<string>  fnameArg("name", "file name", true, "intree", "Input tree file name");
        cmd.add(fnameArg);
        //TCLAP::UnlabeledValueArg<int>  numtreeArg( "numtree", "number of trees", true, 2, "Number of trees"  );
        //cmd.add( numtreeArg );
        TCLAP::SwitchArg blSwitch("l", "branchlength", "include branch length", false);
        cmd.add(blSwitch);
        //TCLAP::ValueArg<int> cArg("c", "cvalue", "c value", false, 1000, "c value");
        //cmd.add( cArg );
        //TCLAP::ValueArg<float> pArg("p", "pvalue", "p value", false, 0.5, "percentage of fixed string");
        //cmd.add( pArg );
        //TCLAP::ValueArg<int> rArg("r", "rvalue", "rate value", false, 50, "rate value");
        //cmd.add( rArg );
        TCLAP::ValueArg<string> outfileArg("o", "outfile", "Output file name", false, "outtree", "Output file name");
        cmd.add(outfileArg);
        //TCLAP::ValueArg<int> seedArg("s", "seedvalue", "user specified seed value", false, 0, "user specified seed value");
        //cmd.add( seedArg );
        cmd.parse(argc, argv);
        //if (seedArg.getValue() != 0) {
        //NEWSEED = seedArg.getValue();
        //cout << "New seed = " << NEWSEED << endl;
        //}
        //if (cArg.getValue() != 1000)
        //C = cArg.getValue();
        //if (pArg.getValue())
        //p = pArg.getValue();
        bBRANCHLEN = blSwitch.getValue();
        outFileName = outfileArg.getValue();
    }
    catch (TCLAP::ArgException &e) {   // catch any exceptions
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    }
    /*********************************************************************************/
    cout << "*** Reading a tree ***\n";
    /*********************************************************************************/
    NEWICKTREE *newickTree;
    int err;
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp) {
        cout << "ERROR: file open error\n";
        exit(0);
    }
    newickTree = loadnewicktree2(fp, &err);
    if (!newickTree) {
        switch (err) {
        case -1:
            printf("Out of memory\n");
            break;
        case -2:
            printf("parse error\n");
            break;
        case -3:
            printf("Can't load file\n");
            break;
        default:
            printf("Error %d\n", err);
        }
        exit(0);
    }
    else {
        int nodeIdx = 0;
        assert(newickTree->root->label != NULL);
        labelInternalNodes(newickTree->root, nodeIdx);
        //printnewicktree(newickTree);
    }
    ofstream fout;
    if (outFileName != "outtree")
        fout.open(outFileName.c_str());
    else
        fout.open("outtree");
    string newickStr = GetTreeString(newickTree->root, bBRANCHLEN, 1.0);
    //cout << newickStr << endl;
    fout << newickStr << endl;
    fout.close();
    cout << "    Completed!\n";
    ///*********************************************************************************/
    //cout << "\n*** Collecting the taxon labels ***\n";
    ///*********************************************************************************/
    //// traverse a tree in dfs order and collect taxon label in lm.
    //// This list is used for determining the location index of taxon label
    //// when generating n-bit bitstring.
    //LabelMap lm;
    //try {
    //GetTaxaLabels2(newickTree->root, lm);
    //}
    //catch (LabelMap::AlreadyPushedEx ex) {
    //cerr << "ERROR: The label '" << ex.label << "' appeard twice in " << endl;
    //exit(2);
    //}
    //NUMTAXA = lm.size();
    //fclose(fp);
    ///*******************************************************************/
    ////COLLECT BIPARTITIONS
    ///*******************************************************************/
    //vector<bitset<BITSETSZ> *> vecBs;
    //unsigned int numBPCollected=0;
    //map<string, double> blTable; // TO RETRIEVE BRANCH LENGTH
    ////printnewicktree(newickTree);
    //DFS(newickTree->root, lm, vecBs, numBPCollected, blTable);
    ////printnewicktree(newickTree);
    //killnewicktree(newickTree);
    ////cout << "    vecBs.size() = " << vecBs.size() << endl;
    //// This is to collect SCNode* for bitsting
    //// For example: when bitstring = 11001
    //// 1. Get the taxon label of '1's => 1st 2nd and 5th
    //// 2. Make 3 SCNode* and insert the pointer in this data structure
    //vector<vector<SCNode*> > vvecDistinctClusters;
    //bitset<BITSETSZ> *bs2 = new bitset<BITSETSZ>;
    //(*bs2).flip();
    //vecBs.push_back(bs2);
    ////if (p > 0.0) {
    //////unsigned totalBP = vecBs.size()-2;
    ////unsigned totalBP = vecBs.size();
    ////vector<int> vecRand;
    ////for (unsigned i = 0; i < totalBP; ++i) {
    ////vecRand.push_back(i);
    ////}
    ////////
    ////random_shuffle(vecRand.begin(), vecRand.end());
    ////////
    ////cout << "    Fix rate=" << p << endl;
    ////unsigned numBPLimit = int(round(totalBP * p));
    ////cout << "    After round=" << round(totalBP * p) << endl;
    ////}
    /////////////
    //// debug
    //////    cout << "str/maj bipartitions: " << vecBs.size() << endl;
    //////    for (unsigned i=0; i<vecBs.size(); ++i)
    //////            cout << *(vecBs[i]) << endl;
    /////////////
    //// debug
    ////////      cout << "collected bipartitions: " << vecBs.size() << endl;
    //////    for (unsigned i=0; i<vecBs.size(); ++i) {
    //////            for (unsigned int j=BITSETSZ-1; j>BITSETSZ-NUMTAXA-1; --j)
    //////                    cout << (*(vecBs[i]))[j];
    //////            cout << endl;
    ////////              cout << *(vecBs[i]) << endl;
    //////    }
    //// NOTE: now vecBs has all bipartitions collected.
    //// Next step is to construct trees from the bitstring information.
    //// Using vvecDistinctClusters, make a list of SCNode* for each bipartition.
    //for (unsigned int i = 0; i < vecBs.size(); ++i) {
    //vector<SCNode*> vecNodes2;
    //unsigned int lmIndex = 0;
    //for (unsigned int j = BITSETSZ-1; j > BITSETSZ-NUMTAXA-1; --j) {
    //if ((*(vecBs[i]))[j]) {
    //SCNode* aNode = new SCNode();
    //aNode->name = lm.name(lmIndex);
    //if (bBRANCHLEN)
    //aNode->bl = blTable[aNode->name];
    //vecNodes2.push_back(aNode);
    //}
    //lmIndex++;
    //}
    //vvecDistinctClusters.push_back(vecNodes2);
    //}
    //// Create multimap with size of the cluster as key.
    //// This is to reorder the list of SCNode* by the number of '1's
    //// in the original bitstirng.
    //// If we process the largest (= biggest number of '1's) first
    //// that makes easy to consruct tree.
    //multimap<unsigned, unsigned, greater<unsigned> > mmapCluster;
    //// Insert the size of distict vector and the index
    //// to sort the distinct vectors by the size of clusters
    //for (unsigned int i = 0; i < vvecDistinctClusters.size(); ++i)
    //mmapCluster.insert(multimap<unsigned,unsigned>::value_type(vvecDistinctClusters[i].size(), i));
    ////////////////////////////////////////////////////////////////////////////////
    //// 09.19.2007
    //// Construct SC tree
    ////////////////////////////////////////////////////////////////////////////////
    //multimap<unsigned,unsigned>::iterator itr;
    //SCTree *scTree = new SCTree();
    //bool addedRoot = false;
    //unsigned int intNodeNum = 0;
    //for (itr = mmapCluster.begin(); itr != mmapCluster.end(); ++itr) {
    //if (!addedRoot) {
    //// The first cluster has all the taxa.
    //// This constructs a star tree with all the taxa.
    //// 1. Dangle all the taxa as root's children by adjusting parent link.
    //// 2. Push all the node* in the root's children
    //// 3. Push all the nodes in the tree's nodeList.
    //// 4. Push all the nodes' parent in the tree's parentlist.
    //for (unsigned int i = 0 ; i < vvecDistinctClusters[itr->second].size(); ++i) {
    //vvecDistinctClusters[itr->second][i]->parent = scTree->root;
    //scTree->root->children.push_back(vvecDistinctClusters[itr->second][i]);
    //scTree->nodeList.push_back(vvecDistinctClusters[itr->second][i]);
    //assert(scTree->nodeList[0]->name == "root");
    //scTree->parentList.insert(map<string,int>::value_type(vvecDistinctClusters[itr->second][i]->name, 0));
    //}
    //addedRoot = true;
    //} else {
    //// For the next biggest cluster,
    //// 1. Find node list to move (= vvecDistinctClusters[itr->second]) and
    ////    Get the parent node of the to-go nodes.
    //// 2. Make an internal node.
    //// 3. Insert the node in the nodeList of the tree and update the parentlist accordingly
    //// 4. Adjust the to-go nodes' parent link.
    //// 5. Adjust the parent node's link to children (delete the moved nodes from children).
    //// 1. --------------------------------------------------------------------------
    //SCNode* theParent = NULL;
    //theParent = scTree->nodeList[scTree->parentList[vvecDistinctClusters[itr->second][0]->name]];
    //assert(theParent != NULL);
    //assert(theParent->name != "");
    //// 2. --------------------------------------------------------------------------
    //string newIntNodeName = "int" + itos(intNodeNum);
    //SCNode* newIntNode = new SCNode();
    //newIntNode->name = newIntNodeName;
    //newIntNode->parent = theParent;
    //// 3. --------------------------------------------------------------------------
    //assert(newIntNodeName.size() != 0);
    //scTree->nodeList.push_back(newIntNode);
    //assert(scTree->nodeList[scTree->nodeList.size()-1]->name == newIntNode->name);
    //scTree->parentList.insert(map<string, unsigned>::value_type(newIntNodeName, scTree->nodeList.size()-1));
    //for (unsigned int i=0; i<vvecDistinctClusters[itr->second].size(); ++i) {
    //// 4. --------------------------------------------------------------------------
    //vvecDistinctClusters[itr->second][i]->parent = newIntNode;
    //// We have to update parentlist in the tree.
    //assert(vvecDistinctClusters[itr->second][i]->parent->name == scTree->nodeList[scTree->nodeList.size()-1]->name);
    //scTree->parentList[vvecDistinctClusters[itr->second][i]->name] = scTree->nodeList.size()-1;
    //newIntNode->children.push_back(vvecDistinctClusters[itr->second][i]);
    //// 5. --------------------------------------------------------------------------
    //// Delete the moved nodes from parent's children.
    //vector<SCNode*>::iterator itr2;
    //for (itr2 = theParent->children.begin(); itr2 != theParent->children.end(); ++itr2) {
    //if (vvecDistinctClusters[itr->second][i]->name == (*itr2)->name) {
    //theParent->children.erase(itr2);
    //break;
    //}
    //}
    //}
    //theParent->children.push_back(newIntNode);
    //intNodeNum++;
    //}
    ////      scTree->DrawOnTerminal();
    //}
    ///*********************************************************************************/
    //ofstream fout;
    //if (outFileName != "outtree")
    //fout.open(outFileName.c_str());
    //else
    //fout.open("outtree");
    //// remove out the extra parenthesis from the Newick string.
    ////string newickStr = scTree->GetTreeString(bBRANCHLEN, 1.0, NEWSEED);
    //string newickStr = scTree->GetTreeString(bBRANCHLEN, 1.0);
    //fout << newickStr << endl;
    //fout.close();
    //cout << "    Completed!\n";
    //mmapCluster.clear();
    //for (unsigned int i=0; i<vvecDistinctClusters.size(); ++i)
    //vvecDistinctClusters[i].clear();
    //// clear cluster table
    //vvecDistinctClusters.clear();
    // CPU time comsumed
    struct rusage a;
    if (getrusage(RUSAGE_SELF, &a) == -1) {
        cerr << "ERROR: getrusage failed.\n";
        exit(2);
    }
    cout << "\n    Total CPU time: " << a.ru_utime.tv_sec + a.ru_stime.tv_sec << " sec and ";
    cout << a.ru_utime.tv_usec + a.ru_stime.tv_usec << " usec.\n";
    return 1;
}

// eof
