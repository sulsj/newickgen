#!/bin/bash
set -ex
topdir=$(pwd)
#echo "Build all..."
#cd hashrfpq/hashrfpq && make clean && ./configure && make  && cd ${topdir}
#cd newickgen/newickgen &&  make clean && ./configure && make  && cd ${topdir}
#echo "Test run..."
echo "Test run (1)..."
python newickgen.py ./test/test.tre test-5-w-bl.tre 5 1
more test-5-w-bl.tre
python newickgen.py ./test/test.tre test-5-wo-bl.tre 5 0
more test-5-wo-bl.tre

echo "Test run (2)..."
python p_arrange.py ./test/test-10trees.tre 10 0.5 test-10trees-w-bl.tre 1
more test-10trees-w-bl.tre
python p_arrange.py ./test/test-10trees.tre 10 0.5 test-10trees-wo-bl.tre 0
more test-10trees-wo-bl.tre

