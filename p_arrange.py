#!/usr/bin/env python

from sys import argv
import sys
from random import *
import subprocess

newickgen="./newickgen/newickgen/newickgen "
#hashrfpq="./hashrfpq/hashrfpq/hashrfpq "

def read_trees(fileName, inputTrees):
  File = open(fileName,"r")
  numline = 1
  for l in File:
    if len(l) == 0:
      break
    inputTrees.append(l)
    numline += 1
  #print "line =", numline      
  File.close()
  #print "num trees in InputTrees =",len(inputTrees)
  return len(inputTrees)
  
def rearrange_newick(l):
    tempTreeFile = "temp.tre"
    f = open("temp.tre", "w")
    f.write(l)
    f.close()
    if (bBranchLen):
        cmd = newickgen + " " + tempTreeFile + " -l "
    else: 
        cmd = newickgen + " " + tempTreeFile 
    
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,)
    stdout_value = proc.communicate()[0]
    s = stdout_value.split()
    #for i in range(len(s)):
		#print str(i) + " " + str(s[i])
    
    outtree = []
    numOuttree = read_trees("outtree", outtree)     
    if (numOuttree != 1):
        print "ERROR: no tree in outtree"
        sys.exit(0)
        
    return outtree
      
if __name__ == "__main__":

	if len(argv) != 6:
		print "usage: python p_arrange.py infile numtree p outfile print_branch_length"
		sys.exit(0)    
		
	inFileName = argv[1]
	numTree = int(argv[2])
	p = float(argv[3])
	outFileName = argv[4]
	bBranchLen = int(argv[5])
	outFile = open(outFileName, "w")
	print "Percentage to rearrange = %0.2f" % (p*100)
	numTreeToRearrange = int(numTree * p)
	print "Total Num trees = %d" % numTree
	print "Num trees to shuffle = %d" % numTreeToRearrange
	if p < 1.0:
		index = range(1, numTree)
		shuffle(index)
		print "Rearranged tree index = ", index[:numTreeToRearrange]
		sid = 0
		for line in open(inFileName, 'r'):
			sid += 1
			if sid in index[:numTreeToRearrange]:
				rearranged = rearrange_newick(line)
				#print rearranged   
				outFile.write(rearranged[0])
			else:
				outFile.write(line)
	elif p == 1.0:
		print "Rearrange all trees in the input file, %s" % inFileName
		sid = 0
		for line in open(inFileName, 'r'):
			sid += 1
			rearranged = rearrange_newick(line)
			outFile.write(rearranged[0])
	else:
		print "ERROR: p value should be 0.0 < p <= 1.0\n"
		sys.exit(0)
		
	print "Completed!"    
	outFile.close()    
