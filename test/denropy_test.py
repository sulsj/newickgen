import random
import dendropy
from dendropy import treecalc
from dendropy import Tree


#def dfs(node):
    #if node.parent_node is None:
        #node.value = start #DISTANCE
    #else:
        #node.value = random.gauss(node.parent_node.value, node.edge.length)
    #for child in node.child_nodes():
        ##print child.taxon.label
        #dfs(child)
    #if node.taxon is not None:
        #print("%s : %s" % (node.taxon, node.value))
        

def process_node(node, start=1.0):
    if node.parent_node is None:
        node.value = start #DISTANCE
    else:
        node.value = random.gauss(node.parent_node.value, node.edge.length)
    for child in node.child_nodes():
        #print child.taxon.label
        process_node(child)
    if node.taxon is not None:
        print("%s : %s" % (node.taxon, node.value))

#mle = dendropy.Tree.get_from_path('pythonidae.mle.nex', 'nexus')
tree1 = dendropy.Tree(stream=open("test.tre"), schema="newick", as_rooted=True)
#process_node(tree1.seed_node)

#print tree1.leaf_nodes()[0].taxon
#print tree1.leaf_nodes()[0].taxon.label

#print treecalc.robinson_foulds_distance(tree1, tree1)

tree1.print_plot()



#mle_len = tree1.length()
for edge in tree1.postorder_edge_iter():
    #pass
    edge.length = 0.0
#print(tree1.as_string("newick"))



tree2 = dendropy.Tree(stream=open("test.tre"), schema="newick", as_rooted=True)
trees = []
trees.append(tree1)
trees.append(tree2)

for edge in tree2.postorder_edge_iter():
    #pass
    edge.length = 0.0

trees.append(tree2)
for tree in trees:
    print(tree.as_string('newick'))

distances = []
for tree in trees:
    distances.append(tree1.robinson_foulds_distance(tree))
    
print distances


tree3 = dendropy.Tree(stream=open("test.tre"), schema="newick", as_rooted=False)
#tree3 = Tree(tree2)
for node in tree3.postorder_node_iter():
    print node.label
