from newick import parse_tree
import newick
import sys
#from phylopy.tree import Tree, Node

class BranchLengthSum(newick.AbstractHandler):
    def __init__(self):
        self.sum = 0.0

    def new_edge(self,b,l):
        if l:
            self.sum += l

    def get_result(self):
        return self.sum

print parse_tree("(A,B);")  

fname = sys.argv[1]
f = open(fname, "r")
line = f.readline()
t = parse_tree(line)
print t
print t.identifier
#print t.root
print newick.parse(line,BranchLengthSum())
